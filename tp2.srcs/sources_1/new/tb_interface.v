`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/01/2020 06:25:56 PM
// Design Name: 
// Module Name: tb_interface
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_interface();

        //LOCAL PARAMS    
        localparam      NB_DATA  = 8;
        localparam      NB_CODE  = 6;
        localparam      NB_STATE = 4;

        //INTERNALS
        reg             clk;
        reg             reset;
        reg             rx_done;                //Indica cuando el receptor termino de recibir el dato.
        reg             [NB_DATA - 1:0] data;             //Dato que tomo el receptor y envia a la ALU.
        
        wire            data_ready;             //Bit para indicar cuando la interfaz tiene listo el calculo de la ALU
        wire            [NB_DATA - 1:0] data_out;         //Dato del calculo de la ALU
        wire            [3:0] state;            
      
      initial begin
                        // Initialize Inputs
                        data = 0;
                        clk = 0;
                        reset = 1;
                        rx_done = 0;
                        #1
                        reset=0;
                        #19
                        data = 8'h01;//carga datoA
                        rx_done = 1;
                        #20
                        rx_done = 0;
                        #20
                        data=8'h08;//carga datoB
                        rx_done = 1;
                        #20
                        rx_done = 0;
                        #20
                        rx_done = 1;
                        data=6'b100000;//carga operacion suma
                        #20
                        rx_done = 0;//-----------Nuevos datos
                        #57 /// Tiempo que tardamos en teclear
                        rx_done = 1;
                        data=8'hf0;//carga datoA
                        #20
                        rx_done = 0;
                        #20
                        rx_done = 1;
                        data=8'hff;//carga datoB
                        #20
                        rx_done = 0;
                        #20
                        rx_done = 1;
                        data=6'b100100;//carga operacion and
                        #20
                        rx_done = 0;
	    end
	
	interface
	#(
	   .NB_DATA (NB_DATA),
	   .NB_CODE (NB_CODE),
	   .NB_STATE (NB_STATE)
	)
	interface_UART_ALU
	(
	   .i_clk(clk),
	   .i_reset(reset),
	   .i_rx_done(rx_done),
	   .i_data(data),
	   .o_data_ready(data_ready),
	   .o_data(data_out),
	   .o_state(state)
	);
	
	always begin //clock de la placa 50Mhz
		#10 clk=~clk;
	end
    
    
            
        
endmodule
