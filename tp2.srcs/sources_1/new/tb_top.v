`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/06/2020 07:33:25 PM
// Design Name: 
// Module Name: tb_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_top();

    // LOCAL_PARAMETERS
    localparam NB_DATA  = 8;
    localparam SB_TICK  = 16;
    localparam NB_CODE  = 6;
    localparam NB_STATE = 4;
    localparam DATA1    = 5;
    localparam DATA2    = 3;
    localparam OP       = 6'b100000;        // SUMA
    
    // TB_SIGNALS
    // COMMONS
    reg                     i_clk;
    reg                     i_reset;
    // TOP
    wire                    i_rx_top;
    wire                    o_tx_top;    
    // UART
    reg     [NB_DATA-1:0]   i_tx_uart;
    reg                     i_tx_start;
    wire                    o_tx_done;
    wire                    o_rx_done;
    wire    [NB_DATA-1:0]   o_rx_uart;
    // AUX
    reg                     test_start;
    
    
    initial begin
        i_clk = 1'b0;
        i_reset = 1'b1;
        test_start = 1'b0;
        i_tx_start = 1'b0;
        
        #10
        i_reset = 1'b0;
                
        #10
        test_start = 1'b1;
        i_tx_start = 1'b1;  
        i_tx_uart = DATA1;
        
        #20
        i_tx_start = 1'b0; 
        
        #1050000
        i_tx_start = 1'b1; 
        i_tx_uart = DATA2;
        
        
        #20
        i_tx_start = 1'b0; 
        
        #1050000
        i_tx_start = 1'b1; 
        i_tx_uart = OP;
        
        #20
        i_tx_start = 1'b0; 
        
        #1050000
             
        $display("############# Test OK ############");
        $finish();
    end
    
    top
    #(
        .NB_DATA        (NB_DATA),
        .NB_CODE        (NB_CODE),
        .SB_TICK        (SB_TICK),
        .NB_STATE       (NB_STATE)
    )
    u_top
    (
        .i_clk          (i_clk),
        .i_reset        (i_reset),
        .i_rx           (i_rx_top),
        .o_tx           (o_tx_top)
    );
    
    uart
    #(
        .NB_DATA          (NB_DATA),
        .SB_TICK          (SB_TICK)
    )
    u_uart
    (
        .i_clk            (i_clk),
        .i_reset          (i_reset), 
        .i_rx             (o_tx_top), 
        .i_tx_start       (i_tx_start),
        .i_tx             (i_tx_uart),
        .o_tx             (i_rx_top),
        .o_tx_done        (o_tx_done),
        .o_rx_done        (o_rx_done),
        .o_rx             (o_rx_uart)
    );
         
   // CLOCK_GENERATION
   always #10 i_clk = ~i_clk;

   // TEST
   always @(posedge i_clk) begin       
       if (o_rx_done) begin            // Verifico datos recibidos
            if (o_rx_uart != (DATA1+DATA2)) begin
                $display("############# Test FALLO ############");
                $finish();
            end
       end
   end
endmodule
