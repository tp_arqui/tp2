`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/01/2020 04:07:19 PM
// Design Name: 
// Module Name: tb_tx
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_tx();
    
    // LOCAL_PARAMETERS
    // BRG
    localparam M        = 326;
    localparam N        = 9;
    // TX
    localparam NB_DATA  = 8;
    localparam SB_TICK  = 16;
    localparam DATA     = 8'b01100101;        
    
    // TB_SIGNALS
    // BRG
    reg                                 clk;
    reg                                 test_start;
    reg                                 reset;
    wire                                tick;
    // TX
    reg                                 tx_start;
    wire                                done_tick;
    reg [NB_DATA-1:0]                   data;
    reg [NB_DATA-1:0]                   aux;
    reg [NB_DATA/2-1:0]                 counter;
    wire                                tx;
    
    initial begin
        clk = 1'b0;
        reset = 1'b1;
        test_start = 1'b0;
        tx_start = 1'b0;
        aux = 0;
        
        
        #10
        reset = 1'b0;
                
        #10
        test_start = 1'b1;
        tx_start = 1'b1;  
        counter = 0;   
        data = DATA;
        
        #20
        tx_start = 1'b0; 
        
        #1041670
        
        $display("############# Test OK ############");
        $finish();
    end
    

    // MODULE_INSTANCE
    // BRG
    baudRateGenerator
    #(
        .M            (M),
        .N            (N)
    )
    u_baudRateGenerator
    (
        .i_clk        (clk),
        .i_reset      (reset),
        .o_tick       (tick)
    );
    // TX
    tx
    #(
        .NB_DATA      (NB_DATA),
        .SB_TICK      (SB_TICK)
    )
    u_tx
    (
        .i_clk        (clk),
        .i_reset      (reset),
        .i_tick       (tick),
        .i_tx_start   (tx_start),
        .i_data       (data),
        .o_tx_done_tick(done_tick),
        .o_tx         (tx)
    );
    
    // CLOCK_GENERATION
    always #10 clk = ~clk;
    
    // TEST
    always @(posedge clk) begin 
        if (tick) begin
            counter = counter + 1;
        end
        if (counter == 0 && tick) begin // Voy concatenando los bits transmitidos
            aux <= {tx, aux[7:1]};
        end
        
        if (done_tick) begin            // Verifico datos transmitidos
             if (DATA != aux) begin
                 $display("############# Test FALLO ############");
                 $finish();
             end
        end
    end
  
endmodule
