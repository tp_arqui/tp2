`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/31/2020 04:58:49 PM
// Design Name: 
// Module Name: tbBaudRateGenerator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tbBaudRateGenerator();
    
    // LOCAL_PARAMETERS
    localparam M = 326;
    localparam N = 9;

    // TB_SIGNALS
    reg                                 clk;
    reg                                 test_start;
    reg                                 reset;
    reg         [N-1:0]                 counter;
    wire                                tick;
    
    initial begin
        clk = 1'b0;
        test_start = 1'b0;
        reset = 1'b1;
        
        #10
        counter = 9'b0;
        reset = 1'b0;
        test_start = 1'b1;
        
        #10000
        
        $display("############# Test OK ############");
        $finish();
    end
    
    // MODULE_INSTANCE
    baudRateGenerator
    #(
        .M            (M),
        .N            (N)
    )
    u_baudRateGenerator
    (
        .i_clk        (clk),
        .i_reset      (reset),
        .o_tick       (tick)
    );

    // CLOCK_GENERATION
    always #10 clk = ~clk;

    // CHECH_MODULE_OUTPUT
    always @(posedge clk) begin
        if (counter == M)
            counter <= 0;
        else
            counter <= counter + 1;
    end
    always @(posedge clk) begin
        if ((counter == M) && (tick == 0)) begin
            $display("############# Test FALLO ############");
            $finish();
        end
    end

endmodule
