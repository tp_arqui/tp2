`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/31/2020 07:24:12 PM
// Design Name: 
// Module Name: tb_rx
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_rx();
    
    // LOCAL_PARAMETERS
    // BRG
    localparam M        = 326;
    localparam N        = 9;
    // RX
    localparam NB_DATA  = 8;
    localparam SB_TICK  = 16;
    localparam RESULT   = 8'b01100101;        
    
    // TB_SIGNALS
    // BRG
    reg                                 clk;
    reg                                 test_start;
    reg                                 reset;
    wire                                tick;
    // RX
    reg                                 rx;
    wire                                done_tick;
    wire [NB_DATA-1:0]                  data;
    
    initial begin
        clk = 1'b0;
        reset = 1'b1;
        test_start = 1'b0;
        rx = 1'b1;      
        
        #10
        reset = 1'b0;
        test_start = 1'b1;
        rx = 1'b0;                      // Bit de START
        
        // (1/9600)*10e9
        #104167
        rx = 1'b1;                      // D0 LSB
        
        #104167
        rx = 1'b0;                      // D1
        
        #104167
        rx = 1'b1;                      // D2
        
        #104167
        rx = 1'b0;                      // D3
        
        #104167
        rx = 1'b0;                      // D4
        
        #104167
        rx = 1'b1;                      // D5
        
        #104167
        rx = 1'b1;                      // D6
        
        #104167
        rx = 1'b0;                      // D7 MSB
        
        #104167
        rx = 1'b1;                      // Bit de Stop
                
        #104167        
        
        $display("############# Test OK ############");
        $finish();
    end
    

    // MODULE_INSTANCE
    // BRG
    baudRateGenerator
    #(
        .M            (M),
        .N            (N)
    )
    u_baudRateGenerator
    (
        .i_clk        (clk),
        .i_reset      (reset),
        .o_tick       (tick)
    );
    // RX
    rx
    #(
        .NB_DATA      (NB_DATA),
        .SB_TICK      (SB_TICK)
    )
    u_rx
    (
        .i_clk        (clk),
        .i_reset      (reset),
        .i_tick       (tick),
        .i_rx         (rx),
        .o_rx_done_tick(done_tick),
        .o_data       (data)
    );
    
    // CLOCK_GENERATION
    always #10 clk = ~clk;
    
    // TEST
    always @(posedge clk) begin
        if ((done_tick) && (RESULT != data)) begin
            $display("############# Test FALLO ############");
            $finish();
        end
    end
  
endmodule