`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/01/2020 04:48:14 PM
// Design Name: 
// Module Name: interface
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module interface
#(
    parameter                   NB_DATA = 8,                    // # data bits
    parameter                   NB_CODE = 6,                    // Bits de codigo
    parameter                   NB_STATE = 4        
)
(
    //INPUTS
    input   wire                i_clk,                           //Clock
                                i_reset,                         //Bit de reset 
                                i_rx_done,                       //Bit que indica que el receptor tiene listo el dato. 
                [NB_DATA-1:0]   i_data,                          //Dato del receptor.
     
    //OUTPUTS
    output  wire                o_data_ready,                    //Bit para indicar que el calculo se realizo correctamente
                [NB_DATA-1:0]   o_data,                          //Dato luego de realizar el calculo. 
                                o_state                          //Indica el estado de la interfaz. Se uso para el TB, tal vez habria que borrarlo.                
);
    //LOCAL PARAMETERS
    localparam                  SAVE_A 		    = 4'b0001;       //Estado donde se guarda el primer operando
    localparam                  SAVE_B 		    = 4'b0010;       //Estado donde se guarda el segundo operando
    localparam                  SAVE_OP         = 4'b0100;       //Estado donde se guarda la operacion
    localparam                  MAKE_RESULT 	= 4'b1000;       //Estado donde se calcula y obtiene el resultado
    
    //INTERNAL
    reg        [NB_DATA-1:0]    data_A;                          //Primer operando de la ALU
    reg        [NB_DATA-1:0]    data_B;                          //Segundo operando de la ALU 
    reg        [NB_CODE-1:0]    operation;                       //Operacion para la ALU 
    reg        [NB_STATE-1:0]   state = SAVE_A;                  //Estado. Inicia en guardar el operando A 
    reg        [NB_STATE-1:0]   next_state = SAVE_B;             //Siguiente estado. Inicia en guardar el operando B. 
    reg        [NB_STATE-1:0]   data_ready;                      //Bit que indica si la ALU termino de calcular el dato.  
    
    assign                      o_data_ready = data_ready;          
    assign                      o_state = state;
   

//Maquina de estados
always @(posedge i_clk, posedge i_reset)
begin
	if(i_reset)                                                   //En el reset vuelvo al primer estado e inicializo los registros.   
		begin
		    data_ready <= 0;
			data_A    <= 8'b00000000;
			data_B    <= 8'b00000000;
			operation <= 5'b00000;
			state     <= SAVE_A;
		end
	else 
		begin
		  if(i_rx_done)
			begin
			  state <= next_state;                                     //Si no es un reset, paso al siguiente estado con cada flanco de clock.
			end
		  case(state)                                                  //Si no es un reset y no vino el rx_done.  
		      MAKE_RESULT:                                             //pero esta en el estado MAKE_RESULT, vuelvo al estado inicial.
		          begin                                                //porque ya paso un clock con el resultado correcto.         
		              state <= next_state;
		          end 
		  endcase
		end
end

always @(*) 
begin
	case (state)
		SAVE_A:                                                       //En el primer estado guardamos el primer operando
              begin 
                data_ready = 1'b0;
                data_A     = i_data;
                next_state = SAVE_B;
              end
		SAVE_B:	                                                      //En el segundo estado guardamos el segundo operando
            begin
                data_ready = 1'b0;
                data_B     = i_data;
                next_state = SAVE_OP;
            end	
		SAVE_OP:                                                      //En el tercer estado guardamos la operacion.  
			begin
			    data_ready = 1'b0; 
                operation  = i_data;
                next_state = MAKE_RESULT;
            end
		MAKE_RESULT:	                                              //En el cuarto estado ponemos en alto la bandera que indica   
            begin                                                     //que el resultado esta listo y volvemos al primer estado.   
                data_ready = 1'b1;
                next_state = SAVE_A;		
            end
		default: 	                                                  //Por default reiniciamos todo y volvemos al primer estado.  
				begin
					next_state = SAVE_A;
					data_A     = 8'b00000000;
			        data_B     = 8'b00000000;
			        operation  = 5'b00000;
				end
	endcase
end

 //ALU
 alu 
    #(
        .NB_DATA    (NB_DATA),
        .NB_DATA_OUT(NB_DATA)
    )
    alu_1 
    (	
        .i_a(data_A), 
        .i_b(data_B),
        .i_op(operation),
        .o_r(o_data)
    );
endmodule
