`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/06/2020 06:32:02 PM
// Design Name: 
// Module Name: tb_uart
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_uart();

    // LOCAL_PARAMETERS
    localparam NB_DATA  = 8;
    localparam SB_TICK  = 16;
    localparam DATA     = 8'b01100101;        
    
    // TB_SIGNALS
    // BRG
    reg                     clk;
    reg                     test_start;
    reg                     reset;
    // UART
    reg     [NB_DATA-1:0]   i_tx;
    reg                     i_tx_start;
    wire                    o_tx;
    wire                    o_tx_done;
    wire                    o_rx_done;
    wire    [NB_DATA-1:0]   o_rx;
    
    initial begin
        clk = 1'b0;
        reset = 1'b1;
        test_start = 1'b0;
        i_tx_start = 1'b0;
        
        
        #10
        reset = 1'b0;
                
        #10
        test_start = 1'b1;
        i_tx_start = 1'b1;  
        i_tx = DATA;
        
        #20
        i_tx_start = 1'b0; 
        
        #1041670
        
        $display("############# Test OK ############");
        $finish();
    end
    
    // UART
    uart
    #(
        .NB_DATA          (NB_DATA),
        .SB_TICK          (SB_TICK)
    )
    u_uart
    (
        .i_clk            (clk),
        .i_reset          (reset), 
        .i_rx             (o_tx), 
        .i_tx_start       (i_tx_start),
        .i_tx             (i_tx),
        .o_tx             (o_tx),
        .o_tx_done        (o_tx_done),
        .o_rx_done        (o_rx_done),
        .o_rx             (o_rx)
    );
        
    // CLOCK_GENERATION
    always #10 clk = ~clk;
    
    // TEST
    always @(posedge clk) begin       
        if (o_rx_done) begin            // Verifico datos transmitidos
             if (DATA != o_rx) begin
                 $display("############# Test FALLO ############");
                 $finish();
             end
        end
    end
endmodule