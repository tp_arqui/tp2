`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/06/2020 07:18:57 PM
// Design Name: 
// Module Name: tb_interfaz
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module tb_interfaz();

        localparam NB_DATA      = 8;
        localparam NB_CODE      = 6;
        localparam NB_STATE     = 4;
        
        reg                     clk;
        reg                     reset;
        reg                     rx_done;
        reg     [NB_DATA-1:0]   data; 
        
        wire                    data_ready;
        wire    [NB_DATA-1:0]   data_out;
        
      initial begin
		// Initialize Inputs
		data = 0;
		clk = 0;
		reset = 1;
		rx_done = 0;
		#1
		reset=0;
		#19
		data = 8'h01;             // Carga datoA
		rx_done = 1;
		#20
		rx_done = 0;
		#20
		data=8'h08;               // Carga datoB
		rx_done = 1;
	 	#20
		rx_done = 0;
		#20
		rx_done = 1;
		data=6'b100000;           // Carga operacion suma
		#20
		rx_done = 0;              // Nuevos datos
		#57                       // Tiempo que tardamos en teclear
		rx_done = 1;
		data=8'hf0;               // Carga datoA
		#20
		rx_done = 0;
		#20
		rx_done = 1;
		data=8'hff;               // Carga datoB
		#20
		rx_done = 0;
		#20
		rx_done = 1;
		data=6'b100100;           // Carga operacion and
		#20
		rx_done = 0;
		
	end
	
	interfaz
	#(
	   .NB_DATA        (NB_DATA),
	   .NB_CODE        (NB_CODE),
	   .NB_STATE       (NB_STATE)
	)
	u_interfaz
	(
	   .i_clk          (clk),
	   .i_reset        (reset),
	   .i_rx_done      (rx_done),
	   .i_data         (data),
	   .o_data_ready   (data_ready),
	   .o_data         (data_out)
	);
	
	always begin                   // Clock de la placa 100Mhz
		#10 clk=~clk;
	end  
endmodule